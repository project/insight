<?php 

function insight_report_form($form, $form_state, $report) {
//dsm($report);
	$report_defs = insight_reports_info();
  
	$title = t('%name report', array('%name' => $report_defs[$report['analyzer']]['title']));
  drupal_set_title($title, PASS_THROUGH);

  $form_state['report'] = $report;
  $form['#attached']['css'][drupal_get_path('module', 'insight') . '/insight.admin.css'] = array();

  if ($report['nid']) {
	  $form['for'] = array(
	    '#type' => 'markup',
	    '#markup' => t('for') . ': ' . l(url('node/' . $report['nid']), 'node/' . $report['nid']),
	  ); 	
  }
   
  // Show the thumbnail preview.
  $form['report'] = array(
    '#type' => 'markup',
    '#markup' => $report['report'],
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $report['active'] ? t('Ignore report') : t('Re-activate report'),
  );
  
  return $form;
}

function insight_report_form_submit($form, $form_state) {
  $report = $form_state['report'];
  $report_defs = insight_reports_info();
  
  $title = t('%name report', array('%name' => $report_defs[$report['type']]['title']));
    
  if ($form_state['values']['submit'] == 'Dismiss report') {
  	insight_report_ignore($report['irid']);
  	$msg = t('%name report has been de-activated for !link', 
  	  array(
  	    '%name' => $report_defs[$report['type']]['title'],
  	    '!link' => l('node/' . $report['nid'], 'node/' . $report['nid']),
  	  )
  	);
  	drupal_set_message($msg);
  }
  else {
  	insight_report_activate($report['irid']);
    $msg = t('%name report has been re-activated for !link', 
      array(
        '%name' => $report_defs[$report['type']]['title'],
        '!link' => l('node/' . $report['nid'], 'node/' . $report['nid']),
      )
    );
    drupal_set_message($msg);
  }
  
  dsm($form_state);
  
  return;
}

function insight_alert_active_operation($alert, $op) {
	//dsm($alert);
	//dsm($op);
	$report_defs = insight_reports_info();
	if (function_exists('insight_alert_' . $op)) {
		call_user_func('insight_alert_' . $op, $alert['iaid']);
		$msg = t('@report_title has been @op.',
		  array(
		    '@report_title' => $report_defs[$alert['analyzer']]['title'],
		    '@op' => ($op == 'dismiss') ? 'removed' : (($op == 'ignore') ? 'marked ignore' : 'activated'),
		  )
		);
		drupal_set_message($msg);
	}
	drupal_goto($_GET['destination']);
}